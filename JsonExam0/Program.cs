﻿using System;

using Newtonsoft.Json;

using Entity;

namespace JsonExam0
{
    class Program
    {
        static void Main(string[] args)
        {
            ExampleUser();
        }

        private static void ExampleUser()
        {
            var original1 = new User
            {
                Name = "Alice",
                Age = 22,
            };

            var json = JsonConvert.SerializeObject(original1);
            Console.WriteLine("JSON:");
            Console.WriteLine(json);
            Console.WriteLine();

            var deserialized = JsonConvert.DeserializeObject<User>(json);
            Console.WriteLine("deserialized:");
            Console.WriteLine($"  Name: {deserialized.Name}");
            Console.WriteLine($"  Age: {deserialized.Age}");
            Console.WriteLine();

            var original2 = new User
            {
                Name = "Bob",
                Age = 21,
            };

            var userList = new UserList();
            userList.Users.Add(original1);
            userList.Users.Add(original2);

            var jsonHasArray = JsonConvert.SerializeObject(userList);
            Console.WriteLine("JSON(has array):");
            Console.WriteLine(jsonHasArray);
            Console.WriteLine();

            var deserializedList = JsonConvert.DeserializeObject<UserList>(jsonHasArray);
            Console.WriteLine("deserialized: [");
            foreach (var u in deserializedList.Users)
            {
                Console.WriteLine("  User:");
                Console.WriteLine($"    Name: {u.Name}");
                Console.WriteLine($"    Age: {u.Age}");
                Console.WriteLine();
            }
            Console.WriteLine("]");
            Console.WriteLine();
        }
    }
}
