﻿using System;
using System.Text;

namespace Common
{
    public class CryptoService
    {
        public static String Encrypt(String rawValue)
        {
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(rawValue));
        }

        public static String Decrypt(String encryptedValue)
        {
            return Encoding.UTF8.GetString(Convert.FromBase64String(encryptedValue));
        }
    }
}
