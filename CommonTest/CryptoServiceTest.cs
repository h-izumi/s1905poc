﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Common;

namespace CommonTest
{
    [TestClass]
    public class CryptoServiceTest
    {
        [TestMethod]
        public void Encrypt()
        {
            var encrypted = CryptoService.Encrypt("Foo");

            Assert.IsInstanceOfType(encrypted, typeof(String));
            Assert.IsTrue(encrypted.Length > 0);
        }

        [TestMethod]
        public void EncryptionAndDecryption()
        {
            var raw = "Foo";

            var encrypted = CryptoService.Encrypt(raw);
            var decrypted = CryptoService.Decrypt(encrypted);

            Assert.AreNotEqual(raw, encrypted);
            Assert.AreNotEqual(encrypted, decrypted);
            Assert.AreEqual(raw, decrypted);
        }
    }
}
