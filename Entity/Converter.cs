﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Entity
{
    public class Converter
    {
        public static string ToJson<T>(T item, Formatting formatting = Formatting.None)
        {
            return Serialize(item, formatting);
        }

        public static string ToXml<T>(T item, Formatting formatting = Formatting.None)
        {
            var json = ToJson(item, Formatting.None);

            var xdoc = JsonConvert.DeserializeXmlNode(json);

            if (formatting == Formatting.Indented)
            {
                using (var sw = new StringWriter())
                {
                    xdoc.Save(sw);
                    return sw.ToString();
                }
            }

            return xdoc.OuterXml;
        }

        public static string Serialize<T>(T item, Formatting formatting = Formatting.None)
        {
            var root = item.GetType().GetCustomAttribute<JsonObjectAttribute>();

            JObject jo;

            if (root is null)
            {
                jo = JObject.FromObject(item);
            }
            else
            {
                jo = new JObject(new JProperty(root.Title, JValue.FromObject(item)));
            }

            return jo.ToString(formatting);
        }
    }
}
