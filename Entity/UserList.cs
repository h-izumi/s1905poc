﻿using System.Collections.Generic;

namespace Entity
{
    public class UserList
    {
        public List<User> Users { get; set; } = new List<User>();
    }
}
