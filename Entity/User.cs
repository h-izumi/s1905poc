﻿using System;

using Newtonsoft.Json;

using Common;

namespace Entity
{
    public class User
    {
        public String EncryptedName { get; set; }

        public String EncryptedAge { get; set; }

        [JsonIgnore]
        public String Name
        {
            get
            {
                return CryptoService.Decrypt(this.EncryptedName);
            }

            set
            {
                this.EncryptedName = CryptoService.Encrypt(value);
            }
        }

        [JsonIgnore]
        public Int32 Age
        {
            get
            {
                if (Int32.TryParse(CryptoService.Decrypt(this.EncryptedAge), out Int32 r))
                {
                    return r;
                }
                return Int32.MinValue;
            }

            set
            {
                this.EncryptedAge = CryptoService.Encrypt(value.ToString());
            }
        }
    }
}
